const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const timestamp = require('mongoose-timestamp');
const { composeWithMongoose } = require('graphql-compose-mongoose');

const StudentSchema = new Schema(
    {
        name: {
            type: String,
            trim: true,
            required: true
        }   
    },
    {
        collection: 'students',
    }
)

StudentSchema.plugin(timestamp);
StudentSchema.index({createdAt: 1, updatedAt: 1});

const Student = mongoose.model('Student', StudentSchema);
const StudentTC = composeWithMongoose(Student);

module.exports = {
    Student,
    StudentTC
}
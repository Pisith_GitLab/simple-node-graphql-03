const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const timestamp = require('mongoose-timestamp');
const { composeWithMongoose } = require('graphql-compose-mongoose');

const StudentRoleSchema = new Schema(
    {
        student: {
            type: Schema.Types.ObjectId,
            ref: 'Student',
            required: true
        },
        role: {
            type: String,
            required: true
        }
    },
    {
        collection: 'StudentRoles',
    }
);

StudentRoleSchema.plugin(timestamp);
StudentRoleSchema.index({createdAt: 1, updatedAt: 2});

const StudentRole = mongoose.model('StudentRole', StudentRoleSchema);
const StudentRoleTC = composeWithMongoose(StudentRole);

module.exports = {
    StudentRole,
    StudentRoleTC
}
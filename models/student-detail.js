const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const timestamp = require('mongoose-timestamp');
const { composeWithMongoose } = require('graphql-compose-mongoose');

const StudentDetailSchema = new Schema(
    {
        student: {
            type: Schema.Types.ObjectId,
            ref: 'Student',
            required: true,
        },
        age: {
            type: Number,
            required: true,
        },
        email: {
            type: String,
            trim: true,
            lowercase: true,
            required: true
        },
        address: {
            type: String,
            required: true
        }
    },
    {
        collection: 'studentDetails'
    }
);

StudentDetailSchema.plugin(timestamp);
StudentDetailSchema.index({createdAt: 1, updatedAd: 1});

const StudentDetail = mongoose.model('StudentDetail', StudentDetailSchema);
const StudentDetailTC = composeWithMongoose(StudentDetail);

module.exports = {
    StudentDetail,
    StudentDetailTC
}
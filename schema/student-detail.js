const { StudentDetail, StudentDetailTC } = require('../models/student-detail');

const StudentDetailQuery = {
    studentDetailById: StudentDetailTC.getResolver('findById'),
    studentDetailMany: StudentDetailTC.getResolver('findMany'),
}

const StudentDetailMutation = {
    studentDetailCreateOne: StudentDetailTC.getResolver('createOne'),
}

module.exports = {
    StudentDetailQuery,
    StudentDetailMutation
}
const { SchemaCompose, SchemaComposer } = require('graphql-compose');

const schemaComposer = new SchemaComposer();

const { StudentQuery, StudentMutation } = require('../schema/student');
const { StudentDetailQuery, StudentDetailMutation } = require('../schema/student-detail');
const { StudentRoleQuery, StudentRoleMutation } = require('../schema/student-role');

schemaComposer.Query.addFields({
    ...StudentQuery,
    ...StudentDetailQuery,
    ...StudentRoleQuery
})

schemaComposer.Mutation.addFields({
    ...StudentMutation,
    ...StudentDetailMutation,
    ...StudentRoleMutation
})

module.exports = schemaComposer.buildSchema();
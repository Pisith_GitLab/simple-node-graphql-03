const { StudentRole, StudentRoleTC } = require('../models/student-role');

const StudentRoleQuery = {
    studentRoleFindById: StudentRoleTC.getResolver('findById'),
    studentRoleFindMany: StudentRoleTC.getResolver('findMany')
}

const StudentRoleMutation = {
    studentRoleCreateOne: StudentRoleTC.getResolver('createOne'),
}

module.exports = {
    StudentRoleQuery,
    StudentRoleMutation
}
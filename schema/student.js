const { Student, StudentTC } = require('../models/student');

const StudentQuery = {
    studentById: StudentTC.getResolver('findById'),
    studentMany: StudentTC.getResolver('findMany')
}

const StudentMutation = {
    studentCreateOne: StudentTC.getResolver('createOne'), 
}

module.exports = {
    StudentQuery,
    StudentMutation
}